FROM fpfis/httpd-php-dev:5.6
RUN curl -sS https://getcomposer.org/installer | php
RUN sudo mv composer.phar /usr/local/bin/
RUN sudo chmod 755 /usr/local/bin/composer.phar
RUN echo 'alias composer="php /usr/local/bin/composer.phar"' >> /root/.bashrc
#RUN source /root/.bashrc
RUN composer -v
#RUN cd /var/www/html || composer install
#RUN ls
#RUN composer install
#RUN cd /var/www/html || ./bin/phing build-platform-dev
#RUN cd /var/www/html || ./bin/phing install-platform